### Upload src to virtual machines

```bash
pscp -r -A -i -h <hosts> src <target dir>/
```
e.g. `pscp -r -A -i -h vms.txt src /home/ubuntu/`


### Create a directory `bin` for `.class` files

```bash
pssh -A -i -h <hosts> mkdir <target dir>/bin
```

### Compile

```bash
pssh -A -i -h <hosts> "javac -d <target dir>/bin <target dir>/src/*.java"
```
e.g. `pssh -A -i -h vms.txt "javac -d bin src/*.java"` assuming <target dir> is the HOME directory.

### Start peer servers

```bash
pssh -A -i -h <hosts> "nohup 'java -cp <target dir>/bin Peer start [conf file]' &"
```
e.g. `pssh -A -i -h vms.txt "nohup 'java -cp bin Peer start ~/star.conf' &"`, where `~/star.conf` is the conf file.

### Check if server running in the background

```bash
pssh -A -i -h <hosts> "ps aux | grep Peer"
```

### Broadcast instructions to servers
View neighbors: `pssh -A -i -h vms.txt "java -cp bin Client neighbor"`

List 10 `.txt` files: `pssh -A -i -h vms.txt "java -cp bin Client list *.txt 10"`
