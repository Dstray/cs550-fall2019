import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * a simple p2p file sharing system
 * @author xxx
 *
 */
public class Peer implements Runnable {

    private InetAddress localHost;
    private final static int TTL_INITIAL_VALUE = 5;
    private final static long QUERY_TIMEOUT_UNIT_MILLIS = 25; // TIME_OUT
    private ServerSocket server = null;
    private Path sharedDir;
    private List<InetSocketAddress> neighbors;
    private int seqNum;
    private HashMap<String, BlockingQueue<QueryResultItem>> qresults;
    private List<QueryResultItem> resList;
    private CircularQueue<String> queryLog; //
    private PrintStream logWriter = null;
    private boolean running = false;
    private boolean test_mode = false;
    
    private int server_mode = 0;
    private InetSocketAddress indexServerAddr;
    private HashMap<InetSocketAddress, HashMap<String, Long>> fileIndex;
    
    /**
     * Bind the peer with the given ID and get to know its neighbors.
     * @param id The specified ID for this peer.
     * @param confdir The directory of the peer's config file.
     */
    public Peer(String conf) {
        super();
        try {
            this.server = new ServerSocket(Config.PORT);
            localHost = InetAddress.getLocalHost();
            System.out.println(localHost.getHostAddress());
            String datestr = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
            OutputStream os = /*System.out.checkError() ? new FileOutputStream(datestr + ".log", true) : */System.out;
            this.logWriter = new PrintStream(os);
            System.setErr(logWriter);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.logWriter.println(new Date().toString());
        System.err.println("Peer Server started.");
        this.running = true;

        if (conf == null) { // index server
            this.server_mode = 1;
            this.fileIndex = new HashMap<>();
            return;
        } else this.initNeighborPeers(new File(conf));
        this.initSharedDir();
        this.seqNum = 1;
        this.qresults = new HashMap<>();
        this.resList = new ArrayList<>();
        this.queryLog = new CircularQueue<>(8);
        if (server_mode == 2)
            register(sharedDir.toFile().listFiles());
    }
    
    public Peer() {
        logWriter = new PrintStream(System.out);
        try {
            localHost = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create data files under the shared directory.
     */
    private void initSharedDir() {
        sharedDir = Paths.get("shared");
        if (sharedDir.toFile().exists())
            return;
        else { // Create files
            sharedDir.toFile().mkdir();
            //TODO
            Random rand = new Random();
            int num, tmp;
            //byte[] dataB = new byte[1000]; // 1KB
            int[] pool = new int[20];
            for (int i = 0; i != 20; i++)
                pool[i] = i;
            for (int i = 0; i != 10; i++) {
                num = i + rand.nextInt(20 - 1 - i);
                tmp = pool[num];
                pool[num] = pool[i];
                pool[i] = tmp;
            }
            
            try {/*
                for (int i = 0; i != 10; i++) { // 10 * 100MB
                    String filename = String.format("%02d.bin", pool[i] % 20);
                    FileOutputStream fis = new FileOutputStream(sharedDir.resolve(filename).toFile());
                    for (int j = 0; j != 100000; j++)
                        fis.write(dataB);
                    fis.close();
                }*/
                for (int i = 0; i != 10; i++) { // 100K * 10KB
                    String filename = String.format("%02d.txt", pool[i]);
                    PrintWriter pw = new PrintWriter(sharedDir.resolve(filename).toFile());
                    for (int j = 0; j != 100 * pool[i]; j++)
                        pw.print("123456789\n");
                    pw.close();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Get to know the neighbor peers
     * @param conf The directory of the peer's config file.
     */
    private void initNeighborPeers(File conf) {
        
        // Read neighbor addresses from config file
        neighbors = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(conf));
            String addr;
            if (conf.getName().startsWith("centralized")) {
                String[] ad = br.readLine().split(":");
                this.indexServerAddr = new InetSocketAddress(ad[0], Integer.parseInt(ad[1]));
                this.server_mode = 2;
            } else while ((addr = br.readLine()) != null) {
                String[] ad = addr.split(":");
                neighbors.add(new InetSocketAddress(ad[0], Integer.parseInt(ad[1])));
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        Socket socket = null;
        try {
            // Shutdown the server
            if (server == null) {
                socket = new Socket(localHost, Config.PORT);
                if (socket.isConnected()) {
                    DataInputStream in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
                    ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                    out.writeUTF("instruction");
                    String[] args = { "shutdown" };
                    out.writeObject(args);
                    out.flush();
                    
                    String resp;
                    while (!(resp = in.readUTF()).equals("$$$$"))
                        logWriter.print(resp);
                    in.close();
                    out.close();
                    socket.close();
                }

            // Start the server.
            } else {
                server.setSoTimeout(1000);
                while (running) {
                    try {
                        socket = server.accept();
                    } catch (SocketTimeoutException e) {
                        // Do nothing.
                    }
                    if (socket != null && socket.isConnected())
                        new MessageParser(socket).start();
                    socket = null;
                }
                server.close();
            }
        } catch (ConnectException e) {
            // Do nothing.
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        if (logWriter != null)
            logWriter.close();
    }
    
    public void obtain(DataOutputStream out, String filename) {
        byte data[] = new byte[1000];
        FileInputStream in;
        try {
            in = new FileInputStream(this.sharedDir.resolve(filename).toFile());
            int len = -1;
            while (-1 != (len = in.read(data)))
                out.write(data, 0, len);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void query(ObjectOutputStream oos, String msgId, String filename) {
        try {
            int cnt = 0;
            printToLog(String.format("Query ID: %s, Filename: %s", msgId, filename));
            for (InetSocketAddress addr: fileIndex.keySet()) {
                HashMap<String, Long> files = fileIndex.get(addr);
                long size = files.getOrDefault(filename, -1L);
                if (size == -1)
                    continue;
                
                oos.writeLong(size);
                oos.writeObject(addr);
                oos.flush();
                cnt++;
            }
            oos.writeLong(-1);
            oos.close();
            printToLog(String.format("<%s> %d files found.", msgId, cnt));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void query(InetSocketAddress upstream, String msgId, int ttl, String filename) {
        
        // Strategy: Ignore the query whose id is the same as one this peer have seen before
        if (queryLog.contains(msgId))
            return;
        queryLog.add(msgId);
        printToLog(String.format("Message ID: %s, TTL: %d, Filename: %s, Upstream Addr: %s\n",
                msgId, ttl, filename, upstream));
        BlockingQueue<QueryResultItem> qrItems = new ArrayBlockingQueue<>(16);
        qresults.put(msgId, qrItems);

        long stime = System.currentTimeMillis();
        // Forward query to all neighbors
        if (ttl > 0) {
            String[] originAd = msgId.split("@")[0].split(":");
            for (InetSocketAddress isa: neighbors) {
                if (isa.equals(upstream) || isa.equals(new InetSocketAddress(originAd[0], Integer.parseInt(originAd[1]))))
                    continue;
                Socket nsocket;
                try {
                    nsocket = new Socket(isa.getAddress(), isa.getPort());
                    if (nsocket.isConnected()) {
                        new QuerySender(nsocket, msgId, filename, ttl - 1).start();
                        printToLog(String.format("  send query<%s> to %s\n", msgId, isa));
                    }
                } catch (ConnectException e) {
                    printToLog(String.format("  Cannot connect to %s\n", isa));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        //Find the file locally
        InetSocketAddress host = new InetSocketAddress(localHost, Config.PORT);
        File file = sharedDir.resolve(filename).toFile();
        if (file.exists())
            qrItems.offer(new QueryResultItem(host, file.getName(), file.length()));
        
        // Start the collector to collect and forward results back
        long duration = System.currentTimeMillis() - stime;
        long timeleft = QUERY_TIMEOUT_UNIT_MILLIS * ttl - duration;
        ResultCollector collector = new ResultCollector(qrItems, msgId, timeleft, upstream);
        collector.start();
        try {
            collector.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        qresults.remove(msgId);
        duration += collector.timecost;
        printToLog(String.format("<%s> Done in %d ms\n", msgId, duration));
    }

    public void queryhit(String msgId, String filename, long len, InetSocketAddress addr) {
        BlockingQueue<QueryResultItem> bq = qresults.get(msgId);
        if (bq == null)
            System.err.println(msgId + " : No such query or query timeout.");
        else
            bq.offer(new QueryResultItem(addr, filename, len));
    }
    
    public void registry(ObjectInputStream in, InetAddress peerAddr) {
        HashMap<String, Long> files = null;
        InetSocketAddress peerAd = null;
        int cnt = 0;
        String filename;
        try {
            peerAd = new InetSocketAddress(peerAddr, in.readInt()); // peerPort
            files = fileIndex.getOrDefault(peerAd, new HashMap<>());
            while (!(filename = in.readUTF()).equals("")) {
                files.put(filename, in.readLong());
                cnt++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        fileIndex.put(peerAd, files);
        printToLog(String.format("%s: registered %d files.", peerAd, cnt));
    }

    public static void main(String[] args) {

        Peer peer = null;
        String conf = null;
        if (args.length == 0) {
            usage();
            return;
        } else {
            if (args[0].equals("end"))
                peer = new Peer();
            else if (args[0].equals("start")) {
                if (args.length > 1)
                    conf = args[1];
                peer = new Peer(conf);
            } else usage();
        }
        
        Thread thread = new Thread(peer);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Execute the instruction received from the client.
     * @param out
     * @param args
     */
    public void performInstruction(DataOutputStream out, String[] args) {
        //TODO
        if (server_mode == 1 && !args[0].equals("shutdown")) {
            responseToClient(out, "$$$$");
            return;
        }
        if (args[0].equals("shutdown")) {
            responseToClient(out, "Server shutdown.\n");
            responseToClient(out, "$$$$");
            running = false;
        } else if (args[0].equals("search")) {
            search(out, args[1]);
        } else if (args[0].equals("download")) {
            download(out, args.length > 1 ? Integer.parseInt(args[1]) : -1);
        } else if (args[0].equals("evaluate")) {
            performEvaluation(out, args);
        } else if (args[0].equals("neighbor")) {
            listNeighbors(out);
        } else if (args[0].equals("display")) {
            printLastSearchResult(out);
        } else if (args[0].equals("list")) {
            String pattern = "*";
            int num = -1;
            if (args.length == 2) {
                try { num = Integer.parseInt(args[1]); }
                catch (NumberFormatException e) { pattern = args[1]; }
            } else if (args.length > 2) {
                pattern = args[1];
                num = Integer.parseInt(args[2]);
            }
            listFiles(out, pattern, num);
        }
        if (!args[0].equals("shutdown"))
            responseToClient(out, "$$$$");
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Print the usage of this client.
     */
    private static void usage() {
        System.out.println("Usage:");
        System.out.println("  Peer start [conf file]    -- Start the server.");
        System.out.println("  Peer end                  -- Shut down the server.");
    }
    
    /**
     * 
     * @param msg
     */
    private void printToLog(String msg) {
        if (!test_mode)
            logWriter.println(msg);
    }
    
    /**
     * Send the message back to the client.
     * @param dos Output stream.
     * @param msg Message.
     */
    private void responseToClient(DataOutputStream dos, String msg) {
        if (dos == null)
            return;
        try {
            dos.writeUTF(msg);
            dos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * List files directly under the shared directory.
     */
    private void listFiles(DataOutputStream dos, String pattern, int num) {
        responseToClient(dos, "File Name \tSize(Bytes)\n");
        String pat = pattern.replace(".", "\\.").replace("*", ".*");
        File[] files = sharedDir.toFile().listFiles((dir, name) -> {
            return Pattern.matches(pat, name);
        });
        for (int i = 0; i != num && i != files.length; i++)
            responseToClient(dos, String.format("%s \t%d\n", files[i], files[i].length()));
    }

    /**
     * List all neighbors of this peer and their status
     */
    private void listNeighbors(DataOutputStream dos) {
        responseToClient(dos, neighbors.size() + " neighbors:\n");
        for (InetSocketAddress isa: neighbors)
            responseToClient(dos, isa + "\n");
    }

    /**
     * Print the result of the last search.
     */
    private void printLastSearchResult(DataOutputStream dos) {
        if (!resList.isEmpty())
            responseToClient(dos, "No. \tPeer Address \tFile Name \tSize(bytes)\n");
        for (int i = 0; i != resList.size(); i++) {
            QueryResultItem qri = resList.get(i);
            responseToClient(dos, String.format("%d \t%s \t%s \t%s\n", 
                    i + 1, qri.hitPeerAddr, qri.fileName, qri.length));
        }
    }
    
    /**
     * Perform batch operations to evaluate performance.
     * @param out Output stream to the client.
     * @param args Arguments of the test.
     */
    private void performEvaluation(DataOutputStream out, String[] args) {
        test_mode = true;
        int n_iters = Integer.parseInt(args[2]), cnt = 0;
        String filename;
        Random rand = new Random();
        long duration = 0;
        long durs[] = new long[n_iters];
        
        // Iteratively repeat search and download operations.
        while (cnt < n_iters) {
            /*if (args.length > 3 && args[3].equals("L"))
                filename = String.format("%02d.bin", rand.nextInt(20));
            else filename = String.format("%06d.txt", rand.nextInt(200000));*/
            filename = String.format("%02d.txt", rand.nextInt(20));
            if (args[1].equals("search")) {
                durs[cnt] = search(null, filename);
                duration += durs[cnt];
            } else if (args[1].equals("download")) {
                durs[cnt] = search(null, filename);
                durs[cnt] += download(null, -1);
                duration += durs[cnt];
            }
            if (cnt++ % 500 == 0) {
                System.err.format("== Evaluation progress: %d/%d\n", cnt, n_iters);
                responseToClient(out, String.format("\r  progress: %d/%d", cnt, n_iters));
            }
        }
        
        // Compute the mean/std of the running time for each trial.
        double mean = (double) duration / n_iters, std = 0;
        for (long dur: durs)
            std += (dur - mean) * (dur - mean);
        std = Math.sqrt(std / (n_iters - 1));
        String rec = String.format("\rEvaluation done in %.3f s.\n", duration / 1000.0);
        rec += String.format("Average: %.2f ms; ", mean);
        rec += String.format("Standard deviation: %.2f ms.\n", std);
        responseToClient(out, rec);
        test_mode = false;
        printToLog(rec);
    }

    /**
     * Download the idx-th file from the corresponding peer.
     * @param dos Output stream to the client.
     * @param idx The sequence number of the item in the result list.
     */
    private long download(DataOutputStream dos, int idx) {
        if (idx > resList.size() || resList.isEmpty()) return 0;
        else if (idx == -1) idx = new Random().nextInt(resList.size()) + 1;
        QueryResultItem qri = resList.get(idx - 1);
        String output;
        
        if (sharedDir.resolve(qri.fileName).toFile().exists()) {
            responseToClient(dos, String.format("%s exists.\n", qri.fileName));
            return 0;
        }
        output = String.format("Download file %s from %s ...\n", qri.fileName, qri.hitPeerAddr);
        printToLog(output);
        responseToClient(dos, output);
        long stime = System.currentTimeMillis();
        try {
            Socket socket = new Socket(qri.hitPeerAddr.getAddress(), qri.hitPeerAddr.getPort());
            DataInputStream in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            FileOutputStream fos = new FileOutputStream(sharedDir.resolve(qri.fileName).toFile());

            //TODO
            out.writeUTF("obtain");
            //obtain(socket, filename);
            out.writeUTF(qri.fileName);
            out.flush();
            
            // Download the file.
            int size = 0, len, cnt = 0;
            byte data[] = new byte[1000];
            while (-1 != (len = in.read(data))) {
                fos.write(data, 0, len);
                size += len;
                if (++cnt % 10000 == 0) {
                    printToLog(String.format("  downloading %s from %s ... (%d/%d)\n", 
                            qri.fileName, qri.hitPeerAddr, size, qri.length));
                    responseToClient(dos, String.format("\r  %.2f%%", (100.0 * size / qri.length)));
                }
            }
            in.close();
            out.close();
            fos.close();
            socket.close();
        } catch (ConnectException e) {
            responseToClient(dos, String.format("Cannot connect to %s.\n", qri.hitPeerAddr));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        long duration = System.currentTimeMillis() - stime;
        printToLog(String.format("Downloading %s from %s costs %d ms.\n", qri.fileName, qri.hitPeerAddr, duration));
        responseToClient(dos, String.format("\rDone in %d ms.\n", duration));
        
        if (server_mode == 2) { // Peer with index server
            File[] files = new File[1];
            files[0] = sharedDir.resolve(qri.fileName).toFile();
            register(files);
        }
        return duration;
    }

    /**
     * Search in other peers for files containing the specified key word
     * @param out Output stream to the client.
     * @param key The key for searching
     */
    private long search(DataOutputStream out, String filename) {
        String msgId = localHost.getHostAddress() + ":" + Config.PORT + "@" + seqNum++;
        printToLog(String.format("Query ID: %s, Filename: %s, Mode: %d\n", msgId, filename, server_mode));
        resList.clear();
        
        long duration;
        if (server_mode == 0)
            duration = searchAmongPeers(out, msgId, filename);
        else
            duration = searchIndexServer(msgId, filename);
        
        // Display or record the searching result.
        if (resList.isEmpty())
            responseToClient(out, "Not found.\n");
        else
            responseToClient(out, "No. \tPeer Address \tFile Name \tSize(bytes)\n");
        for (int i = 0; i != resList.size(); i++) {
            QueryResultItem qri = resList.get(i);
            responseToClient(out, String.format("\r%d \t%s \t%s \t%s\n> ", 
                    i + 1, qri.hitPeerAddr, qri.fileName, qri.length));
        }
        responseToClient(out, String.format("Done in %d ms\n", duration));
        printToLog(String.format("<%s> Done in %d ms\n", msgId, duration));
        return duration;
    }
    
    /**
     * Perform search by broadcasting queries to neighbor peers.
     * @param out Output stream to the client.
     * @param msgId Message ID of this query.
     * @param filename Name of the file searching for.
     * @return Time in milliseconds costed to perform searching.
     */
    private long searchAmongPeers(DataOutputStream out, String msgId, String filename) {
        long stime = System.currentTimeMillis();
        BlockingQueue<QueryResultItem> qrItems = new ArrayBlockingQueue<>(16);
        qresults.put(msgId, qrItems);
        
        // Broadcast the query to neighbor peers.
        for (InetSocketAddress isa: neighbors) {
            Socket socket;
            try {
                socket = new Socket(isa.getAddress(), isa.getPort());
                if (socket.isConnected()) {
                    new QuerySender(socket, msgId, filename, TTL_INITIAL_VALUE).start();
                    printToLog(String.format("  send query<%s> to %s\n", msgId, isa));
                }
            } catch (ConnectException e) {
                responseToClient(out, String.format("Cannot connect to %s.\n", isa));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        long duration = System.currentTimeMillis() - stime;
        
        // Start the collector to wait for responses.
        ResultCollector collector = new ResultCollector(qrItems, msgId, 
                QUERY_TIMEOUT_UNIT_MILLIS * TTL_INITIAL_VALUE, null);
        collector.start();
        try {
            collector.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        qresults.remove(msgId);
        return duration + collector.timecost;
    }
    
    /**
     * Perform search by querying the index server.
     * @param Message ID of this query.
     * @param filename Name of the file searching for.
     * @return Time in milliseconds costed to perform searching.
     */
    private long searchIndexServer(String msgId, String filename) {
        long stime = System.currentTimeMillis();
        try {
            Socket socket = new Socket(indexServerAddr.getAddress(), indexServerAddr.getPort());
            ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            
            // Send "query" request to the index server. 
            out.writeUTF("query");
            //query(out, filename);
            out.writeUTF(msgId);
            out.writeUTF(filename);
            out.flush();
            
            // Receive the results from the index server.
            long len;
            InetSocketAddress addr;
            ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
            while ((len = in.readLong()) >= 0) {
                addr = (InetSocketAddress) in.readObject();
                resList.add(new QueryResultItem(addr, filename, len));
            }
            
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return System.currentTimeMillis() - stime;
    }
    
    /**
     * Register local files to the index server.
     */
    private void register(File[] files) {
        try {
            Socket socket = new Socket(indexServerAddr.getAddress(), indexServerAddr.getPort());
            ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            
            int cnt = 0;
            out.writeUTF("registry");
            out.writeInt(Config.PORT);
            for (File file: files) {
                out.writeUTF(file.getName());
                out.writeLong(file.length());
                out.flush();
                if (++cnt % 5000 == 0)
                    printToLog(String.format("%d files registered to index server ...", cnt));
            }
            out.writeUTF("");
            printToLog(String.format("All %d files registered to index server.", cnt));
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Handle the requests coming to this server according to the protocol.
     */
    private class MessageParser extends Thread {
        
        private Socket socket;
        
        public MessageParser(Socket socket) {
            this.socket = socket;
        }
        
        @Override
        public void run() {
            try {
                ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
                
                String msgType = in.readUTF();
                if (msgType.equals("instruction")) {
                    String[] args = (String[]) in.readObject();
                    performInstruction(new DataOutputStream(new BufferedOutputStream(socket.getOutputStream())), args);
                    
                } else if (msgType.equals("query")) {
                    int upstreamPort = server_mode == 1 ? 0 : in.readInt();
                    String msgId = in.readUTF();
                    int ttl = server_mode == 1 ? 0 : in.readInt();
                    String filename = in.readUTF();
                    if (server_mode == 1)
                        query(new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream())), msgId, filename);
                    else query(new InetSocketAddress(socket.getInetAddress(), upstreamPort), msgId, ttl, filename);
                    
                } else if (msgType.equals("hitquery")) {
                    String msgId = in.readUTF();
                    String filename = in.readUTF();
                    long len = in.readLong();
                    InetSocketAddress addr = (InetSocketAddress) in.readObject();
                    queryhit(msgId, filename, len, addr);
                    
                } else if (msgType.equals("obtain")) {
                    String filename = in.readUTF();
                    obtain(new DataOutputStream(new BufferedOutputStream(socket.getOutputStream())), filename);
                    
                } else if (msgType.equals("registry")) {
                    registry(in, socket.getInetAddress());
                    in.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Send query to a specified peer.
     */
    private class QuerySender extends Thread {
        
        Socket socket;
        String msgId;
        String filename;
        int ttl;
        
        QuerySender(Socket socket, String msgId, String filename, int ttl) {
            this.socket = socket;
            this.msgId = msgId;
            this.filename = filename;
            this.ttl = ttl;
        }
        
        @Override
        public void run() {
            try {
                //TODO
                ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                out.writeUTF("query");
                //query(socket, msgId, ttl, filename);
                out.writeInt(Config.PORT);
                out.writeUTF(msgId);
                out.writeInt(ttl);
                out.writeUTF(filename);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Collect query results and show them out.
     */
    private class ResultCollector extends Thread {

        BlockingQueue<QueryResultItem> qrItems;
        String msgId;
        long timeout;
        InetSocketAddress upstream;
        long timecost = 0;
        
        /**
         * Default constructor.
         * @param qrItems The queue where we take result items.
         * @param msgId Message ID of the query.
         * @param timeout Wait at most this milliseconds for the results.
         * @param upstream The id of the upstream peer for this query.
         */
        public ResultCollector(BlockingQueue<QueryResultItem> qrItems,
                String msgId, long timeout, InetSocketAddress upstream) {
            this.qrItems = qrItems;
            this.msgId = msgId;
            this.timeout = timeout;
            this.upstream = upstream;
        }
        
        @Override
        public void run() {
            long timeleft = timeout;
            try {
                QueryResultItem qri;
                long stime = System.currentTimeMillis();
                while (null != (qri = qrItems.poll(timeleft, TimeUnit.MILLISECONDS))) {
                    timeleft -= System.currentTimeMillis() - stime;
                    
                    // Show result or forward it back to upstream peer
                    printToLog(String.format("Hit Message ID: %s, File Name: %s, From: %s\n",
                            msgId, qri.fileName, qri.hitPeerAddr));
                    this.collect(qri);
                    
                    stime = System.currentTimeMillis();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timecost = timeout - timeleft;
        }

        /**
         * Add qri to the result list.
         * @param qri An item of query results.
         */
        private void collect(QueryResultItem qri) {
            if (upstream == null) {
                resList.add(qri);
                return;
            }
            try {
                Socket socket = new Socket(upstream.getAddress(), upstream.getPort());
                ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                //TODO
                out.writeUTF("hitquery");
                //queryhit(msgId, filename, len, addr);
                out.writeUTF(msgId);
                out.writeUTF(qri.fileName);
                out.writeLong(qri.length);
                out.writeObject(qri.hitPeerAddr);
                
                out.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

class QueryResultItem {

    InetSocketAddress hitPeerAddr;
    String fileName;
    long length;
    
    /**
     * Default constructor.
     * @param addr The address of the peer where the file locates.
     * @param name The name of the target file.
     * @param length The size of the file in bytes.
     * @param downstreamId The id of the downstream peer who sent this result back.
     */
    public QueryResultItem(InetSocketAddress addr, String name, long length) {
        this.hitPeerAddr = addr;
        this.fileName = name;
        this.length = length;
    }
}

class CircularQueue<T> {
    
    private T [] queue;
    private int size;
    private int idx;
    
    @SuppressWarnings("unchecked")
    public CircularQueue(int size) {
        this.queue = (T[]) new Object[size];
        this.size = size;
        this.idx = 0;
    }
    
    public boolean contains(T val) {
        for (T elem: queue)
            if (val == elem || val.equals(elem))
                return true;
        return false;
    }
    
    /**
     * The eldest element will be replaced if queue is full.
     * @param val The new element.
     */
    public void add(T val) {
        queue[idx] = val;
        if (++idx == size)
            idx = 0;
    }
}