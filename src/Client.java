import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Random;

public class Client {
    
    private Socket socket = null;
    private PrintStream logWriter = null;
    private ObjectOutputStream oos;
    private DataInputStream dis;
    private static HashSet<String> commands;
    
    public Client() {
        try {
            InetAddress localhost = InetAddress.getLocalHost();
            logWriter = new PrintStream(System.out);
            socket = new Socket(localhost, Config.PORT);
            oos = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            dis = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            commands = new HashSet<>();
        } catch (ConnectException e) {
            logWriter.println("Connection refused.");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        commands.add("search");
        commands.add("download");
        commands.add("evaluate");
        commands.add("neighbor");
        commands.add("list");
        commands.add("display");
        commands.add("shutdown");
        commands.add("help");
        commands.add("quit");
    }
    
    public boolean isConnected() {
        return socket != null && socket.isConnected();
    }
    
    private void sendInstruction(String[] args) {
        try {
            
            oos.writeUTF("instruction");
            oos.writeObject(args);
            oos.flush();
            
            String resp;
            while (!(resp = dis.readUTF()).equals("$$$$"))
                logWriter.print(resp);
            
        } catch (IOException e) {
            e.printStackTrace();
        } 
    }
    
    public void run() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        for (;;) {
            logWriter.print("> ");
            String cmd = null;
            try {
                cmd = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (cmd == null) break;
            String[] args = cmd.split("\\s+");
            if (!commands.contains(args[0]))
                continue;
            
            if (args[0].equals("quit"))
                break;
            else if (args[0].equals("help"))
                usage();
            else sendInstruction(args);
        }
    }
    
    public void run(String[] args) {
        if (args[0].equals("quit"))
            return;
        else if (args[0].equals("help"))
            usage();
        else sendInstruction(args);
    }
    
    public void close() {
        try {
            socket.close();
            oos.close();
            dis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        logWriter.close();
    }

    public void usage() {
        System.out.println("Usage:");
        System.out.println("  search <filename>         -- search for a file with its name");
        System.out.println("  download [No.]            -- download a file in the last search result");
        System.out.println("  evaluate <search|download> <n_iterations> [file type]");
        System.out.println("  neighbor                  -- list neighbors of this peer");
        System.out.println("  list [pattern] [number]   -- list files under the shared directory");
        System.out.println("  display                   -- display results of the last search");
        System.out.println("  quit                      -- quit this client");
        System.out.println("  help                      -- display this message");
        System.out.println("  shutdown                  -- shutdown the server");
    }

    public static void main(String[] args) {
        /*Random rand = new Random();
        int num, tmp;
        int[] pool = new int[400];
        for (int i = 0; i != 400; i++)
            pool[i] = i;
        for (int i = 0; i != 200; i++) {
            num = i + rand.nextInt(400 - 1 - i);
            tmp = pool[num];
            pool[num] = pool[i];
            pool[i] = tmp;
        }
        for (int i = 0; i != 200; i++) {
            System.out.format("%4d", pool[i]);
            if (i % 10 == 9)
                System.out.println();
        }
        return;*/
        
        Client client = new Client();
        if (!client.isConnected())
            return;
        if (args.length > 0)
            client.run(args);
        else
            client.run();
        client.close();
    }

}
